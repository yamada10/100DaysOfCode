# Data Structures & Algorithms

### Book

- Genetic Algorithms with Python

### Website

- [Project Euler](https://projecteuler.net/)
- [Data Structures & Algorithms (Python School)](https://pythonschool.net/category/data-structures-algorithms.html)

# Machine Learning

### Book

- ニューラルネットワーク自作入門

### Website

- [Practical Machine Learning Tutorial with Python](https://pythonprogramming.net/machine-learning-tutorial-python-introduction/)

# Functional Programming

- Functional Programming in Python (Packt)
- Iterators in Functional Programming with Python (Packt)
