{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Currying - One argument per function\n",
    "\n",
    "## Let's take a simple function\n",
    "\n",
    "Most functions, even simple ones, take multiple arguments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1110\n"
     ]
    }
   ],
   "source": [
    "def add(a, b, c):\n",
    "    \n",
    "    return a + b + c\n",
    "\n",
    "print(add(10,100,1000))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Binding function arguments\n",
    "\n",
    "But we can 'bind' arguments to a function, so that we end up with a function that takes one argument less than the original function. This is done with `functools.partial()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1110\n"
     ]
    }
   ],
   "source": [
    "from functools import partial\n",
    "\n",
    "add_10 = partial(add, 10)\n",
    "add_10_100 = partial(add_10, 100)\n",
    "print(add_10_100(1000))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Currying\n",
    "\n",
    "*Currying* is a specific kind of argument binding, in which we create a sequence of functions that each take exactly one argument. In Python, you can implement this elegantly with a decorator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1110\n"
     ]
    }
   ],
   "source": [
    "from inspect import signature\n",
    "\n",
    "def curry(fnc):\n",
    "    \n",
    "    def inner(arg):\n",
    "        \n",
    "        if len(signature(fnc).parameters) == 1:\n",
    "            return fnc(arg)\n",
    "        return curry(partial(fnc, arg))\n",
    "    \n",
    "    return inner\n",
    "        \n",
    "    \n",
    "@curry\n",
    "def add(a, b, c):\n",
    "    \n",
    "    return a + b + c\n",
    "\n",
    "\n",
    "add_10 = add(10)\n",
    "add_10_100 = add_10(100)\n",
    "print(add_10_100(1000))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Monads - Variables that decide how they should be treated\n",
    "\n",
    "Let's start with some function, `camelcase()`, which transform `'a_string_like_this'` into `'AStringLikeThise'`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "SomeFunction\n"
     ]
    }
   ],
   "source": [
    "def camelcase(s):\n",
    "    \n",
    "    return ''.join([w.capitalize() for w in s.split('_')])\n",
    "\n",
    "print(camelcase('some_function'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Maybe monad\n",
    "\n",
    "The Maybe monad consists of two kinds of data, which are typically called `Just` and `Nothing`. They both behave very simply:\n",
    "\n",
    "- When a function is bound to a `Just` value, the function is simply executed, and the result is stored in a new `Just` value.\n",
    "- When a function is bound to a `Nothing` value, the function is bypassed, and `Nothing` is returned right away.\n",
    "- In additon, when a function generates an error, it returns a `Nothing` value.\n",
    "\n",
    "See how similar this is to `nan` behavior?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "SomeFunction\n",
      "Nothing\n",
      "Nothing\n"
     ]
    }
   ],
   "source": [
    "class Just:\n",
    "    \n",
    "    def __init__(self, value):\n",
    "        \n",
    "        self._value = value\n",
    "        \n",
    "    def bind(self, fnc):\n",
    "        \n",
    "        try:\n",
    "            return Just(fnc(self._value))\n",
    "        except:\n",
    "            return Nothing()\n",
    "    \n",
    "    def __repr__(self):\n",
    "        \n",
    "        return self._value\n",
    "    \n",
    "\n",
    "\n",
    "class Nothing:\n",
    "    \n",
    "    def bind(self, fnc):\n",
    "        \n",
    "        return Nothing()\n",
    "    \n",
    "    def __repr__(self):\n",
    "        \n",
    "        return 'Nothing'\n",
    "    \n",
    "    \n",
    "print(Just('some_function').bind(camelcase))\n",
    "print(Nothing().bind(camelcase))\n",
    "print(Just(10).bind(camelcase))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The List monad\n",
    "\n",
    "The `List` monad stores a list of values. When it is bound to a function, each value is passed onto the function separately, and the result is stored as another `List`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['SomeText', 'MoreText']"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "class List:\n",
    "    \n",
    "    def __init__(self, values):\n",
    "        \n",
    "        self._values = values\n",
    "        \n",
    "    def bind(self, fnc):\n",
    "        \n",
    "        return List([fnc(value) for value in self._values])\n",
    "    \n",
    "    def __repr__(self):\n",
    "        \n",
    "        return str(self._values)\n",
    "    \n",
    "    \n",
    "List(['some_text', 'more_text']).bind(camelcase)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Memoization - Remembering results\n",
    "\n",
    "## Some functions are expensive\n",
    "\n",
    "Let's consider a function that can take a long time to execute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "999983\n"
     ]
    }
   ],
   "source": [
    "def prime(n):\n",
    "    \n",
    "    for i in range(n, 0, -1):\n",
    "        if all([i // x != i / x for x in range(i-1, 1, -1)]):\n",
    "            return i\n",
    "        \n",
    "print(prime(1000000))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Caching\n",
    "\n",
    "We can speed up function calls by storing the result in a cache."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "999983\n",
      "999983\n"
     ]
    }
   ],
   "source": [
    "cache = {}\n",
    "\n",
    "def cached_prime(n):\n",
    "    \n",
    "    if n in cache:\n",
    "        return cache[n]    \n",
    "    for i in range(n, 0, -1):\n",
    "        if all([i // x != i / x for x in range(i-1, 1, -1)]):\n",
    "            cache[n] = i\n",
    "            return i\n",
    "        \n",
    "print(cached_prime(1000000))\n",
    "print(cached_prime(1000000))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Memoization\n",
    "\n",
    "Memoization is a type of caching in which return values are stored for specific arguments. Therefore, the implementation above is an example of memoization. But it can be implemented more elegantly using a decorator!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "999983\n",
      "999983\n"
     ]
    }
   ],
   "source": [
    "def memoize(fnc):\n",
    "    \n",
    "    cache = {}\n",
    "    \n",
    "    def inner(*args):\n",
    "        \n",
    "        if args in cache:\n",
    "            return cache[args]\n",
    "        cache[args] = fnc(*args)\n",
    "        return cache[args]\n",
    "    \n",
    "    return inner\n",
    "\n",
    "\n",
    "@memoize\n",
    "def memoized_prime(n):\n",
    "    \n",
    "    for i in range(n, 0, -1):\n",
    "        if all([i // x != i / x for x in range(i-1, 1, -1)]):\n",
    "            return i\n",
    "        \n",
    "\n",
    "print(memoized_prime(1000000))\n",
    "print(memoized_prime(1000000))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# You cannot catch Exceptions in lambda expressions\n",
    "\n",
    "## Exceptions\n",
    "\n",
    "In Python, `Exception`s are the main way to deal with errors. Whenever an error occurs, an `Exception` object is 'raised'. This causes the code to abort, until the Exception is caught in a `try: … except: …` statement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "None\n"
     ]
    }
   ],
   "source": [
    "def add_str(s):\n",
    "    \n",
    "    try:\n",
    "        return sum([int(i) for i in s.split('+')])\n",
    "    except AttributeError:\n",
    "        return None\n",
    "\n",
    "print(add_str(1+2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The curse of statements\n",
    "\n",
    "\n",
    "But `try` is a *statement*, and you can therefore not use it in `lambda` expressions! Unlike for most other statements, Python does not offer an expression alternative to the `try` statement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "ename": "AttributeError",
     "evalue": "'int' object has no attribute 'split'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mAttributeError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-11-edc1b84a1cc6>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[0ml_add_str\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;32mlambda\u001b[0m \u001b[0ms\u001b[0m\u001b[0;34m:\u001b[0m \u001b[0msum\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0mint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mi\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;32mfor\u001b[0m \u001b[0mi\u001b[0m \u001b[0;32min\u001b[0m \u001b[0ms\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0msplit\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'+'\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      2\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 3\u001b[0;31m \u001b[0mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0ml_add_str\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;36m1\u001b[0m\u001b[0;34m+\u001b[0m\u001b[0;36m2\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;32m<ipython-input-11-edc1b84a1cc6>\u001b[0m in \u001b[0;36m<lambda>\u001b[0;34m(s)\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0ml_add_str\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;32mlambda\u001b[0m \u001b[0ms\u001b[0m\u001b[0;34m:\u001b[0m \u001b[0msum\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0mint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mi\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;32mfor\u001b[0m \u001b[0mi\u001b[0m \u001b[0;32min\u001b[0m \u001b[0ms\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0msplit\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'+'\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m      2\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      3\u001b[0m \u001b[0mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0ml_add_str\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;36m1\u001b[0m\u001b[0;34m+\u001b[0m\u001b[0;36m2\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mAttributeError\u001b[0m: 'int' object has no attribute 'split'"
     ]
    }
   ],
   "source": [
    "l_add_str = lambda s: sum([int(i) for i in s.split('+')])\n",
    "\n",
    "print(l_add_str(1+2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Handling errors in lambda expressions\n",
    "\n",
    "## Let's reconsider our lambda"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "l_add_str = lambda s: sum([int(i) for i in s.split('+')])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A Maybe-like decorator\n",
    "\n",
    "The Maybe monad is not very Pythonic. But we can do something similar using a decorator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "'int' object has no attribute 'split'\n"
     ]
    }
   ],
   "source": [
    "def maybe(fnc):\n",
    "    \n",
    "    def inner(*args):\n",
    "        \n",
    "        for a in args:\n",
    "            if isinstance(a, Exception):\n",
    "                return a\n",
    "        try:\n",
    "            return fnc(*args)\n",
    "        except Exception as e:\n",
    "            return e\n",
    "        \n",
    "    return inner\n",
    "\n",
    "safe_add_str = maybe(lambda s: sum([int(i) for i in s.split('+')]))\n",
    "print(safe_add_str(1+2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exceptions are fine!\n",
    "\n",
    "Even though `Exception`s are not entirely compatible with a functional programming style, they are still a very good way to deal with errors!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A fully functional, interactive calculator\n",
    "\n",
    "## Our functional calculator … so far\n",
    "\n",
    "This is the calculator that we implemented in the first section. But it suffers from a few drawbacks:\n",
    "\n",
    "- No input validation\n",
    "- No looping"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Enter an integer: 4\n",
      "Enter an operator (+, -, *, /): *\n",
      "Enter an integer: 6\n",
      "The result is: 24\n"
     ]
    }
   ],
   "source": [
    "OPERATORS = '+', '-', '*', '/'\n",
    "\n",
    "\n",
    "def f_get_number():\n",
    "    return int(input('Enter an integer: '))\n",
    "\n",
    "\n",
    "def f_get_operator():\n",
    "    return input('Enter an operator (+, -, *, /): ')\n",
    "\n",
    "\n",
    "def f_calculate(number1, operator, number2):\n",
    "    return number1+number2 if operator == '+' \\\n",
    "        else number1-number2 if operator == '-' \\\n",
    "        else number1/number2 if operator == '/' \\\n",
    "        else number1*number2 if operator == '*' \\\n",
    "        else None    \n",
    "\n",
    "\n",
    "def f_main():\n",
    "    return f_calculate(\n",
    "        f_get_number(),\n",
    "        f_get_operator(),\n",
    "        f_get_number(),\n",
    "        )\n",
    "\n",
    "\n",
    "print('The result is: %s' % f_main())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Let's get to work!\n",
    "\n",
    "Our toolkit contains:\n",
    "\n",
    "- Lambda expressions\n",
    "- Decorators\n",
    "- Higher-order functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Enter an integer: 5\n",
      "Enter an operator (+, -, *, /): -\n",
      "Enter an integer: 5\n",
      "0\n",
      "Enter an integer: 3\n",
      "Enter an operator (+, -, *, /): *\n",
      "Enter an integer: 3\n",
      "9\n"
     ]
    },
    {
     "ename": "KeyboardInterrupt",
     "evalue": "",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mKeyboardInterrupt\u001b[0m                         Traceback (most recent call last)",
      "\u001b[0;32m/Applications/anaconda/lib/python3.6/site-packages/ipykernel/kernelbase.py\u001b[0m in \u001b[0;36m_input_request\u001b[0;34m(self, prompt, ident, parent, password)\u001b[0m\n\u001b[1;32m    729\u001b[0m             \u001b[0;32mtry\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 730\u001b[0;31m                 \u001b[0mident\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mreply\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0msession\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mrecv\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mstdin_socket\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;36m0\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    731\u001b[0m             \u001b[0;32mexcept\u001b[0m \u001b[0mException\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/Applications/anaconda/lib/python3.6/site-packages/jupyter_client/session.py\u001b[0m in \u001b[0;36mrecv\u001b[0;34m(self, socket, mode, content, copy)\u001b[0m\n\u001b[1;32m    795\u001b[0m         \u001b[0;32mtry\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 796\u001b[0;31m             \u001b[0mmsg_list\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0msocket\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mrecv_multipart\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mmode\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mcopy\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mcopy\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    797\u001b[0m         \u001b[0;32mexcept\u001b[0m \u001b[0mzmq\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mZMQError\u001b[0m \u001b[0;32mas\u001b[0m \u001b[0me\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/Applications/anaconda/lib/python3.6/site-packages/zmq/sugar/socket.py\u001b[0m in \u001b[0;36mrecv_multipart\u001b[0;34m(self, flags, copy, track)\u001b[0m\n\u001b[1;32m    394\u001b[0m         \"\"\"\n\u001b[0;32m--> 395\u001b[0;31m         \u001b[0mparts\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;34m[\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mrecv\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mflags\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mcopy\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mcopy\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mtrack\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mtrack\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    396\u001b[0m         \u001b[0;31m# have first part already, only loop while more to receive\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32mzmq/backend/cython/socket.pyx\u001b[0m in \u001b[0;36mzmq.backend.cython.socket.Socket.recv (zmq/backend/cython/socket.c:7683)\u001b[0;34m()\u001b[0m\n",
      "\u001b[0;32mzmq/backend/cython/socket.pyx\u001b[0m in \u001b[0;36mzmq.backend.cython.socket.Socket.recv (zmq/backend/cython/socket.c:7460)\u001b[0;34m()\u001b[0m\n",
      "\u001b[0;32mzmq/backend/cython/socket.pyx\u001b[0m in \u001b[0;36mzmq.backend.cython.socket._recv_copy (zmq/backend/cython/socket.c:2344)\u001b[0;34m()\u001b[0m\n",
      "\u001b[0;32m/Applications/anaconda/lib/python3.6/site-packages/zmq/backend/cython/checkrc.pxd\u001b[0m in \u001b[0;36mzmq.backend.cython.checkrc._check_rc (zmq/backend/cython/socket.c:9621)\u001b[0;34m()\u001b[0m\n",
      "\u001b[0;31mKeyboardInterrupt\u001b[0m: ",
      "\nDuring handling of the above exception, another exception occurred:\n",
      "\u001b[0;31mKeyboardInterrupt\u001b[0m                         Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-15-a0eae33b8824>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     54\u001b[0m \u001b[0mmain_loop\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mrepeat\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;32mlambda\u001b[0m\u001b[0;34m:\u001b[0m \u001b[0mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mmain\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0muntil\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mforever\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     55\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 56\u001b[0;31m \u001b[0mmain_loop\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;32m<ipython-input-15-a0eae33b8824>\u001b[0m in \u001b[0;36minner\u001b[0;34m(*args)\u001b[0m\n\u001b[1;32m     27\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     28\u001b[0m         \u001b[0;32mwhile\u001b[0m \u001b[0;32mTrue\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 29\u001b[0;31m             \u001b[0mresult\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mfnc\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m*\u001b[0m\u001b[0margs\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     30\u001b[0m             \u001b[0;32mif\u001b[0m \u001b[0muntil\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mresult\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     31\u001b[0m                 \u001b[0;32mreturn\u001b[0m \u001b[0mresult\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-15-a0eae33b8824>\u001b[0m in \u001b[0;36m<lambda>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     52\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     53\u001b[0m \u001b[0mforever\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;32mlambda\u001b[0m \u001b[0mretval\u001b[0m\u001b[0;34m:\u001b[0m \u001b[0;32mFalse\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 54\u001b[0;31m \u001b[0mmain_loop\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mrepeat\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;32mlambda\u001b[0m\u001b[0;34m:\u001b[0m \u001b[0mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mmain\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0muntil\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mforever\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     55\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     56\u001b[0m \u001b[0mmain_loop\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-15-a0eae33b8824>\u001b[0m in \u001b[0;36m<lambda>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     46\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     47\u001b[0m main = lambda: calculate(\n\u001b[0;32m---> 48\u001b[0;31m         \u001b[0msafe_get_number\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     49\u001b[0m         \u001b[0msafe_get_operator\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     50\u001b[0m         \u001b[0msafe_get_number\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-15-a0eae33b8824>\u001b[0m in \u001b[0;36minner\u001b[0;34m(*args)\u001b[0m\n\u001b[1;32m     27\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     28\u001b[0m         \u001b[0;32mwhile\u001b[0m \u001b[0;32mTrue\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 29\u001b[0;31m             \u001b[0mresult\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mfnc\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m*\u001b[0m\u001b[0margs\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     30\u001b[0m             \u001b[0;32mif\u001b[0m \u001b[0muntil\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mresult\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     31\u001b[0m                 \u001b[0;32mreturn\u001b[0m \u001b[0mresult\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-15-a0eae33b8824>\u001b[0m in \u001b[0;36minner\u001b[0;34m(*args)\u001b[0m\n\u001b[1;32m     12\u001b[0m                 \u001b[0;32mreturn\u001b[0m \u001b[0ma\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     13\u001b[0m         \u001b[0;32mtry\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 14\u001b[0;31m             \u001b[0;32mreturn\u001b[0m \u001b[0mfnc\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m*\u001b[0m\u001b[0margs\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     15\u001b[0m         \u001b[0;32mexcept\u001b[0m \u001b[0mException\u001b[0m \u001b[0;32mas\u001b[0m \u001b[0me\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     16\u001b[0m             \u001b[0;32mreturn\u001b[0m \u001b[0me\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-15-a0eae33b8824>\u001b[0m in \u001b[0;36m<lambda>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     35\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     36\u001b[0m \u001b[0mis_int\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;32mlambda\u001b[0m \u001b[0mi\u001b[0m\u001b[0;34m:\u001b[0m \u001b[0misinstance\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mi\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mint\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 37\u001b[0;31m \u001b[0mget_number\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;32mlambda\u001b[0m\u001b[0;34m:\u001b[0m \u001b[0mint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0minput\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'Enter an integer: '\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     38\u001b[0m \u001b[0msafe_get_number\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mrepeat\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mmaybe\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mget_number\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0muntil\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mis_int\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     39\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/Applications/anaconda/lib/python3.6/site-packages/ipykernel/kernelbase.py\u001b[0m in \u001b[0;36mraw_input\u001b[0;34m(self, prompt)\u001b[0m\n\u001b[1;32m    703\u001b[0m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_parent_ident\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    704\u001b[0m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_parent_header\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 705\u001b[0;31m             \u001b[0mpassword\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;32mFalse\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    706\u001b[0m         )\n\u001b[1;32m    707\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m/Applications/anaconda/lib/python3.6/site-packages/ipykernel/kernelbase.py\u001b[0m in \u001b[0;36m_input_request\u001b[0;34m(self, prompt, ident, parent, password)\u001b[0m\n\u001b[1;32m    733\u001b[0m             \u001b[0;32mexcept\u001b[0m \u001b[0mKeyboardInterrupt\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    734\u001b[0m                 \u001b[0;31m# re-raise KeyboardInterrupt, to truncate traceback\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 735\u001b[0;31m                 \u001b[0;32mraise\u001b[0m \u001b[0mKeyboardInterrupt\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    736\u001b[0m             \u001b[0;32melse\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    737\u001b[0m                 \u001b[0;32mbreak\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mKeyboardInterrupt\u001b[0m: "
     ]
    }
   ],
   "source": [
    "OPERATORS = '+', '-', '*', '/'\n",
    "\n",
    "\n",
    "def maybe(fnc):\n",
    "    \n",
    "    \"\"\"Turns Exceptions into return values.\"\"\"\n",
    "    \n",
    "    def inner(*args):\n",
    "        \n",
    "        for a in args:\n",
    "            if isinstance(a, Exception):\n",
    "                return a\n",
    "        try:\n",
    "            return fnc(*args)\n",
    "        except Exception as e:\n",
    "            return e\n",
    "        \n",
    "    return inner\n",
    "\n",
    "\n",
    "def repeat(fnc, until):\n",
    "    \n",
    "    \"\"\"Repeats a function until its return value meets\n",
    "    the stop criterion.\"\"\"\n",
    "    \n",
    "    def inner(*args):\n",
    "\n",
    "        while True:\n",
    "            result = fnc(*args)\n",
    "            if until(result):\n",
    "                return result\n",
    "            \n",
    "    return inner\n",
    "\n",
    "\n",
    "is_int = lambda i: isinstance(i, int)\n",
    "get_number = lambda: int(input('Enter an integer: '))\n",
    "safe_get_number = repeat(maybe(get_number), until=is_int)\n",
    "\n",
    "is_operator = lambda o: o in OPERATORS\n",
    "get_operator = lambda: input('Enter an operator (+, -, *, /): ')\n",
    "safe_get_operator = repeat(get_operator, until=is_operator)\n",
    "\n",
    "\n",
    "calculate = lambda number1, operator, number2: \\\n",
    "    number1+number2 if operator == '+' \\\n",
    "    else number1-number2 if operator == '-' \\\n",
    "    else number1/number2 if operator == '/' \\\n",
    "    else number1*number2 if operator == '*' \\\n",
    "    else None    \n",
    "\n",
    "main = lambda: calculate(\n",
    "        safe_get_number(),\n",
    "        safe_get_operator(),\n",
    "        safe_get_number(),\n",
    "        )\n",
    "\n",
    "forever = lambda retval: False\n",
    "main_loop = repeat(lambda: print(main()), until=forever)\n",
    "\n",
    "main_loop()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [default]",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
