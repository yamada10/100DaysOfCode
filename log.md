# 100 Days Of Code - Log

### Day 1: Dec 17th, 2017

**進捗:** [Data Structures & Algorithms (Python School)](https://pythonschool.net/category/data-structures-algorithms.html) から、Linear Search, Binary Search, Bubble Sort。

**感想:** Bubble Sort を書けなかった。ノートに整理しておこう。

### Day 2: Dec 18th, 2017

**進捗:** First Recurring. K Nearest Neighbors Classifier.

**感想:** KNNは以前一度読んだものの再訪。以前よりもすんなりわかったような気がする。スクラッチでは書けないが。

### Day 3: Dec 19th, 2017

**進捗:** Project Euler(1~3).

**感想:** 3問目、Largest prime factor の解答はでたけど、何かおかしいので後日再訪する。

### Day 4: Dec 23th, 2017

**進捗:** Project Euler(3). "Functional Programming in Python"(~3).

**感想:** "Project Euler" 3問目を再訪。整理されてわかりやすくなった。"Functional Programming in Python"と、その続きのコースを一巡したら、"A Concrete Introduction to Probability (using Python)" を再訪したい。

### Day 5: Dec 24th, 2017

**進捗:** Project Euler(4~6,8). "Functional Programming in Python". Bubble Sort. "Iterators in Functional Programming with Python".

**感想:** "Project Euler" 解けたけれど、乱暴な解き方なので後日他を考えてみることにする。／"Functional Programming in Python" の、関数が入れ子になる辺りは頭が追いつかない。とりあえず眺めるだけにしておいた。／毎日1つ、アルゴリズムのコードをノートに収録して、それを繰り返し眺めるということをする。まずは、Bubble Sort から。／"Iterators in Functional Programming with Python" の前半を眺める。

### Day 6: Dec 25th, 2017

**進捗:** "Iterators in Functional Programming with Python". "Genetic Algorithms with Python"

**感想:** "Iterators in Functional Programming with Python"のノートブックのコードを拾う。何に使うか不明のものは飛ばした。／"Genetic Algorithms with Python" を読みはじめた。最初の章のコードを明日書く。

### Day 7: Dec 26th, 2017

**進捗:** 『ニューラルネットワーク自作入門』／PE（7）

**感想:** 『ニューラルネットワーク自作入門』の説明部を読む。説明部をもう一度、ノートに少しまとめて、それからコード部に移る。／"Genetic Algorithms with Python" は今日はできなかった。／朝起きて2時間の支出で、基本的には。／PEは10,001番目の素数を求めよ、という問題。解けなかった。後日再訪する。

### Day 8: Dec 27th, 2017

**進捗:** PE（7,9,15）／"Genetic Algorithms with Python"

**感想:** PEの10問目は、200万までの数字のうちの素数を全部足すと？　という問題。これまでのやり方だと終わらないので、numpyのチュートリアルを明日以降やってみることにする。／"Genetic Algorithms with Python" の最初のコードを写す。微妙にわからないので明日以降自前で書き直してみる。本のコードはその後の使用も考えて書かれてるけど、その辺も端折ってよりシンプルにして。

### Day 9: Dec 28th, 2017

**進捗:** PE（25）／『ニューラルネットワーク自作入門』

**感想:** 『自作入門』のコードを少し書く。途中で詰まる。


### Day 10: Dec 29th, 2017

**進捗:** "GA"

**感想:** "GA"の最初のコードを再訪。分からなかった部分は分かった。
